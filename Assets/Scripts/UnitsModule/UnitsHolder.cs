﻿using Assets.Scripts.UnitsModule;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Управление всеми игроками на сцене
/// </summary>
public class UnitsHolder : MonoBehaviour
{
    private const string playerTag = "Player";

    [SerializeField]
    private List<Unit> unitsOnScene = new List<Unit>();

    [SerializeField]
    private Spawner _spawner;

    private static Player _player;

    public int unitsCount { get => unitsOnScene.Count; }
    public Spawner spawner { get => _spawner; }


    public void Born(Unit unit)
    {
        unitsOnScene.Add(unit);
    }

    public void Death(Unit unit)
    {

    }

    public static Player GetPlayer()
    {
        if (_player == null)
        {
            var gameObject = GameObject.FindGameObjectWithTag(playerTag);
            _player = gameObject.GetComponent<Player>();
        }

        return _player;
    }
}
