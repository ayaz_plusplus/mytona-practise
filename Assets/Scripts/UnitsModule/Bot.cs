﻿using UnityEngine;

namespace Assets.Scripts.UnitsModule
{
    public class Bot : Unit
    {
        protected override void Start()
        {
            nickname = GetRandomNickName();
            base.Start();     
        }

        private string GetRandomNickName()
        {
            return $"Player{Random.Range(0, 1000)}";
        }
    }
}
