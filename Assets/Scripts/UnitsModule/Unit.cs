﻿using Assets.Scripts.Settings;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UnitsModule
{
    /// <summary>
    /// Базовый класс юнита в игре
    /// </summary>
    public abstract class Unit : MonoBehaviour, IUnit, IUser
    {
        [SerializeField]
        private GameObject _unitCanvas;

        [SerializeField]
        private TextMeshProUGUI _nickname;

        [SerializeField]
        private FractionData _fractionData;

        [SerializeField]
        private Spawner _spawner;

        [SerializeField]
        private SoldierAnimator _soldierAnimator;

        private Shooting _shooting;
        private Health _health;
        private CapsuleCollider _collider;
        private Rigidbody _rigidbody;
        private EventsPublisher _eventsPublisher;
        private int _id;
        private byte _murdersAmount;
        private byte _deathsAmount;

        public Health health { get => _health; }

        public string nickname { get => _nickname.text; set => _nickname.text = value; }
        public FractionData fractionData { get => _fractionData; set => _fractionData = value; }
        public EventsPublisher eventsPublisher { get => _eventsPublisher; set => _eventsPublisher = value; }
        public int id { get => _id; set => _id = value; }
        public byte murdersAmount { get => _murdersAmount; set => _murdersAmount = value; }
        public byte deathsAmount { get => _deathsAmount; set => _deathsAmount = value; }
        public SoldierAnimator soldierAnimator { get => _soldierAnimator; set => _soldierAnimator = value; }
        public Spawner spawner { get => _spawner; set => _spawner = value; }
        public Shooting shooting { get => _shooting; set => _shooting = value; }

        public event Action<Unit> OnBorned = delegate { };
        public event Action<Unit> OnDestroyed = delegate { };

        protected virtual void Start()
        {
            _shooting = GetComponent<Shooting>();
            _soldierAnimator.ChangeAnimation(SoldierAnimator.SoldierAnimatorStates.Idle);
            _health = GetComponent<Health>();
            _collider = GetComponent<CapsuleCollider>();
            _rigidbody = GetComponent<Rigidbody>();
            _nickname.color = fractionData.color;
            eventsPublisher = EventsPublisher.instance;
            eventsPublisher.AddInfo(this, $"{nickname} {LocalizationHolder.localeNotification.unitConnected}");
            OnBorned(this);
        }

        public void Disable()
        {
            OnDestroyed(this);
            StartCoroutine(_spawner.Respawn(this));
        }

        /// <summary>
        /// Возродить или отключить игрока
        /// </summary>
        /// <param name="isActive"></param>
        public void SetActiveUnit(bool isActive)
        {
            _rigidbody.isKinematic = !isActive;
            _collider.enabled = isActive;
            _unitCanvas.SetActive(isActive);
            foreach (Transform child in transform)
                child.gameObject.SetActive(isActive);
        }
    }
}
