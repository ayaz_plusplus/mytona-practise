﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.UnitsModule
{
    public interface IUser
    {
        int id { get; set; }
        byte murdersAmount { get; set; }
        byte deathsAmount { get; set; }
    }
}
