﻿using Assets.Scripts.UnitsModule;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Управление появлением ботов
/// </summary>
public class Spawner : MonoBehaviour
{
    [SerializeField]
    private float _respawnDelay = 5f;

    [SerializeField]
    private int _maxBotCount = 5;

    [SerializeField]
    private GameObject _enemyPrefab;

    private List<Transform> _respawnPoints = new List<Transform>();
    private UnitsHolder _unitsHolder;

    private void Start()
    {
        _unitsHolder = GetComponent<UnitsHolder>();

        foreach (Transform child in transform)
            _respawnPoints.Add(child);
        StartCoroutine(EnemyRespawnTask());
    }

    /// <summary>
    /// Генерация ботов
    /// </summary>
    /// <returns></returns>
    private IEnumerator EnemyRespawnTask()
    {
        for (; ; )
        {
            if (_unitsHolder.unitsCount < _maxBotCount)
            {
                var newEnemy = Instantiate(_enemyPrefab, GetRandomPoint(), _enemyPrefab.transform.rotation);
                var unit = newEnemy.transform.GetComponentsInChildren<Unit>().FirstOrDefault();
                unit.OnBorned += _unitsHolder.Born;
                unit.OnDestroyed += _unitsHolder.Death;
                unit.id = _unitsHolder.unitsCount;
                unit.spawner = this;
            }

            yield return new WaitForSeconds(_respawnDelay);
        }
    }

    /// <summary>
    /// Возрождение
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public IEnumerator Respawn(Unit unit)
    {
        unit.SetActiveUnit(false);
        unit.transform.position = GetRandomPoint();

        yield return new WaitForSeconds(_respawnDelay);

        unit.SetActiveUnit(true);
        unit.health.ResetHealth();   
    }

    /// <summary>
    /// Получить случайную точку респауна
    /// </summary>
    /// <returns></returns>
    private Vector3 GetRandomPoint()
    {
        var randomRespawnPoint = _respawnPoints[Random.Range(0, _respawnPoints.Count)];
        return new Vector3(randomRespawnPoint.position.x, randomRespawnPoint.position.y, randomRespawnPoint.transform.position.z);
    }
}
