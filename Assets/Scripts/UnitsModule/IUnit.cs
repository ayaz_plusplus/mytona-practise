﻿namespace Assets.Scripts.UnitsModule
{
    interface IUnit : IDisable
    {
        Health health { get; }

    }
}
