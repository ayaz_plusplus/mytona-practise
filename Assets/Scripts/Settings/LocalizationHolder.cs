﻿using Assets.Scripts.Settings.LocaleData;
using Newtonsoft.Json;
using System;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using static TMPro.TMP_Dropdown;

namespace Assets.Scripts.Settings
{
    /// <summary>
    /// Управление локализацией 
    /// </summary>
    public class LocalizationHolder : MonoBehaviour
    {
        [SerializeField]
        private TMP_Dropdown _languageList;

        [SerializeField]
        private string _localizationPath = "Localization";

        [SerializeField]
        private TMP_Text languageHeaderText;

        private event UnityAction<int> OnLanguageSelected = delegate { };

        private string localizationPath { get => $"{Application.streamingAssetsPath}/{_localizationPath}"; }

        private string currentLanguageDirectory { get => $"{localizationPath}/{currentOptionData.text}"; }

        private OptionData currentOptionData { get => _languageList.options[_languageList.value]; }

        public event Action OnLanguageChanged = delegate { };

        public static LocaleNotification localeNotification = new LocaleNotification();
        public static LocaleUI localeUI = new LocaleUI();


        private void Awake()
        {
            OnLanguageSelected = delegate { LanguageInitialize(); };

            _languageList.onValueChanged.AddListener(OnLanguageSelected);
            FillLanguageList(localizationPath);
            LanguageInitialize();
            OnLanguageChanged += PropertiesReload;
            PropertiesReload();
        }

        private void OnDestroy()
        {
            _languageList.onValueChanged.RemoveListener(OnLanguageSelected);
        }

        /// <summary>
        /// Сериализовать из json-файла в экземпляр класса
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="to"></param>
        /// <param name="jsonFilePath"></param>
        private void Serialize<T>(ref T to, string jsonFilePath)
        {
            string jsonData = File.ReadAllText(jsonFilePath);
            to = JsonConvert.DeserializeObject<T>(jsonData);
        }

        /// <summary>
        /// Инициализировать тексты выбранного языка
        /// </summary>
        public void LanguageInitialize()
        {
            // при добавлении нового тематического источника данных добавить его здесь
            Serialize(ref localeNotification, GetJsonPath<LocaleNotification>());
            Serialize(ref localeUI, GetJsonPath<LocaleUI>());
            OnLanguageChanged();
        }

        /// <summary>
        /// Получить путь к текущему json файлу имя класса = имя файла
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private string GetJsonPath<T>()
        {
            return $"{currentLanguageDirectory}/{typeof(T).Name}.json";
        }

        /// <summary>
        /// Заполнить выподающий список всеми доступными вариантами языков
        /// </summary>
        /// <param name="directoryName"></param>
        private void FillLanguageList(string directoryName)
        {
            var directories = Directory.GetDirectories(directoryName);
            foreach (var directoryPath in directories)
            {
                var dir = new DirectoryInfo(directoryPath);
                _languageList.options.Add(new TMP_Dropdown.OptionData(dir.Name));
            }
        }


        /// <summary>
        /// Обновить значения текстов в полях
        /// </summary>
        private void PropertiesReload()
        {
            languageHeaderText.text = localeUI.languageHeader;
        }
    }
}
