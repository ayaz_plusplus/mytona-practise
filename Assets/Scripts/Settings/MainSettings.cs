﻿using UnityEngine;

public class MainSettings : MonoBehaviour
{
    public void isActive()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
