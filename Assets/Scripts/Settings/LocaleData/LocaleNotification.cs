﻿namespace Assets.Scripts.Settings.LocaleData
{
    public class LocaleNotification
    {
        public string unitConnected { get; set; }
        public string killedHimself { get; set; }
    }
}
