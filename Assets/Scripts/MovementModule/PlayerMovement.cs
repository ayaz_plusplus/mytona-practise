﻿using UnityEngine;

namespace Assets.Scripts.MovementModule
{
    /// <summary>
    /// Реализация передвижения игрока
    /// </summary>
    public class PlayerMovement : Movement
    {
        [SerializeField]
        private Cursor _cursor;

        [SerializeField]
        private float _jumpForce = 1f;

        [SerializeField]
        private float _speed = 0.3f;

        [SerializeField]
        private float _maxSpeed = 3f;

        private bool isGrounded
        {
            get
            {
                var bottomCenterPoint = new Vector3(_collider.bounds.center.x, _collider.bounds.min.y, _collider.bounds.center.z);

                //создаем невидимую физическую капсулу и проверяем не пересекает ли она обьект который относится к полу

                //_collider.bounds.size.x / 2 * 0.9f -- эта странная конструкция берет радиус обьекта.
                // был бы обязательно сферой -- брался бы радиус напрямую, а так пишем по-универсальнее

                return Physics.CheckCapsule(_collider.bounds.center, bottomCenterPoint, _collider.bounds.size.x / 2 * 0.9f, _groundLayer);
                // если можно будет прыгать в воздухе, то нужно будет изменить коэфициент 0.9 на меньший.
            }
        }

        /// <summary>
        /// Направление к курсору
        /// </summary>
        protected override Vector3 destination
        {
            get
            {

                if (_cursor != null)
                    _destination = _cursor.transform.position;


                return _destination;
            }
            set => base.destination = value;
        }

        public Vector3 movementVector
        {
            get
            {
                float moveHorizontal = Input.GetAxis("Horizontal");
                float moveVertical = Input.GetAxis("Vertical");

                return new Vector3(moveHorizontal, 0.0f, moveVertical);
            }
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            JumpLogic();
        }

        private void JumpLogic()
        {
            if (isGrounded && Input.GetButtonDown("Jump"))
            {
                _rigidBody.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);
            }
        }

        public override void MovementLogic()
        {
            if (movementVector != Vector3.zero)
            {
                _rigidBody.AddForce(movementVector * _speed, ForceMode.Impulse);
                _unit.soldierAnimator.ChangeAnimation(SoldierAnimator.SoldierAnimatorStates.Run);
            }
            else
                _unit.soldierAnimator.ChangeAnimation(SoldierAnimator.SoldierAnimatorStates.Idle);

            if (_rigidBody.velocity.magnitude >= _maxSpeed)
            {
                _rigidBody.velocity = _rigidBody.velocity.normalized * _maxSpeed;
            }
        }
    }
}
