﻿using Assets.Scripts.ShootingModule;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.MovementModule
{
    /// <summary>
    /// Реализация перемещений ботов
    /// </summary>
    public class BotMovement : Movement
    {
        [SerializeField]
        private BotShooting _botShooting;

        [SerializeField]
        private LayerMask _playerLayer;

        [SerializeField]
        private float _sightRange = 10f;

        private bool _enemySightRange;
        private NavMeshAgent _navMeshAgent;

        /// <summary>
        /// Направление движения бота
        /// </summary>
        protected override Vector3 destination
        {
            get
            {
                if (UnitsHolder.GetPlayer() != null && _unit.shooting != null)
                    _destination = UnitsHolder.GetPlayer().transform.position; 

                return _destination;
            }
            set => base.destination = value;
        }

        /// <summary>
        /// В зоне видимости враг или нет
        /// </summary>
        public bool enemySightRange { get => _enemySightRange; set => _enemySightRange = value; }

        protected override void Start()
        {
            base.Start();

            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        protected override void FixedUpdate()
        {
            var from = _botShooting.aim.position;
            var to = new Vector3(destination.x, from.y, destination.z);
            // враг должен быть в пределах видимости для стрельбы
            var enemyInSight = _botShooting.CheckCollision(to, false);
            enemySightRange = Physics.CheckSphere(transform.position, _sightRange, _playerLayer) && enemyInSight != null;
            base.FixedUpdate();
        }

        /// <summary>
        /// Переопределение базовой логики движения, вызывается в FixedUpdate
        /// </summary>
        public override void MovementLogic()
        {
            if (_navMeshAgent != null && !_health.isDeath && !enemySightRange)
            {
                _navMeshAgent.SetDestination(destination);
                _unit.soldierAnimator.ChangeAnimation(SoldierAnimator.SoldierAnimatorStates.Run);
            }
            else
            {
                Stop();
            }
        }

        /// <summary>
        /// Остановить движение бота
        /// </summary>
        private void Stop()
        {
            _navMeshAgent.ResetPath();
            _unit.soldierAnimator.ChangeAnimation(SoldierAnimator.SoldierAnimatorStates.Idle);
        }
    }
}
