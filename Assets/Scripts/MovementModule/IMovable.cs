﻿namespace Assets.Scripts.MovementModule
{
    public interface IMovable
    {
        void MovementLogic();
        void LookAt();
    }
}
