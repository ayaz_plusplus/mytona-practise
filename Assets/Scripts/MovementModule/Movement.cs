﻿using Assets.Scripts.MovementModule;
using Assets.Scripts.UnitsModule;
using UnityEngine;

/// <summary>
/// Базовый класс передвижения для юнитов
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public abstract class Movement : MonoBehaviour, IMovable
{
    protected Vector3 _destination;

    [SerializeField]
    protected LayerMask _groundLayer;

    [SerializeField]
    protected Unit _unit;
    protected Health _health;
    protected Rigidbody _rigidBody;
    protected CapsuleCollider _collider;
    protected bool isMove;

    protected virtual Vector3 destination { get => _destination; set => _destination = value; }

    protected virtual void Start()
    {
        _health = GetComponent<Health>();
        _rigidBody = GetComponent<Rigidbody>();
        _collider = GetComponent<CapsuleCollider>();


        //т.к. нам не нужно что бы персонаж мог падать сам по-себе без нашего на то указания.
        //то нужно заблочить поворот по осях X и Z
        _rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        if (_groundLayer == gameObject.layer)
            Debug.LogError("Player SortingLayer must be different from Ground SourtingLayer!");
    }

    protected virtual void FixedUpdate()
    {
        MovementLogic();
        LookAt();
    }

    public virtual void LookAt()
    {
        Vector3 forward = destination - transform.position;
        transform.rotation = Quaternion.LookRotation(new Vector3(forward.x, 0, forward.z));
    }

    public virtual void MovementLogic()
    {
      
    }
}
