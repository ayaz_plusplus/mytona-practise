﻿using UnityEngine;

/// <summary>
/// Отслеживание игрока камерой
/// </summary>
public class PlayerObserver : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    Vector3 offset;

    private void Start()
    {
        offset = transform.position - _target.position;
    }

    private void LateUpdate()
    {
        transform.position = _target.position + offset;
    }
}
