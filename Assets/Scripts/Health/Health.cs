﻿using Assets.Scripts.Scriptable_Objects;
using Assets.Scripts.Settings;
using Assets.Scripts.UnitsModule;
using System;
using UnityEngine;

/// <summary>
/// Управление здоровьем игровых объектов
/// </summary>
public class Health : MonoBehaviour
{
    [SerializeField]
    private float _maxHealth = 100;

    [SerializeField]
    private float _currentHealth;

    private Unit _currentUnit;

    public float currentHealth { get => _currentHealth; set => _currentHealth = value; }
    public bool isDeath { get => _currentHealth <= 0;  }
    public event Action<float> OnHealthPercentChanged = delegate { };
    public event Action<float> OnHealthRestored = delegate { };

    private void OnEnable()
    {
        currentHealth = _maxHealth;
        _currentUnit = GetComponent<Unit>();    
    }

    /// <summary>
    /// Получить урон от оружия
    /// </summary>
    /// <param name="attacker"></param>
    /// <param name="weaponData"></param>
    public void HitWeapon(Unit attacker, IWeaponData weaponData)
    {
        currentHealth -= weaponData.damage;

        if (isDeath)
        {
            if (attacker.id != _currentUnit.id)
                _currentUnit.eventsPublisher.AddMurderInfo(attacker, _currentUnit, weaponData);
            else
                _currentUnit.eventsPublisher.AddInfo(_currentUnit, $"{_currentUnit.nickname} {LocalizationHolder.localeNotification.killedHimself}"); 

            _currentUnit.Disable();
        }
        else
        {
            float currentHealthPercent = currentHealth / _maxHealth;
            OnHealthPercentChanged(currentHealthPercent);
        }
    }

    /// <summary>
    /// Восстановить здоровье
    /// </summary>
    public void ResetHealth()
    {
        currentHealth = _maxHealth;
        float currentHealthPercent = currentHealth / _maxHealth;
        OnHealthRestored(currentHealthPercent);
    }
}
