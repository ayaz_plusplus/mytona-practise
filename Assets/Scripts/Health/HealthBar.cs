﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Управление индикатором здоровья
/// </summary>
public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private float _updateSpeedSeconds = 0.5f;

    [SerializeField]
    private Health _health;

    private Image _foregroundImage;  

    private void Awake()
    {
        _health.OnHealthPercentChanged += HandleHealthChanged;
        _health.OnHealthRestored += HandleHealthRestored;
        _foregroundImage = GetComponent<Image>();
    }

    private void OnDestroy()
    {
        _health.OnHealthPercentChanged -= HandleHealthChanged;
        _health.OnHealthRestored -= HandleHealthRestored;
    }

    private void HandleHealthChanged(float percent)
    {
        if (gameObject.activeSelf)
            StartCoroutine(ChangeToPercent(percent));
    }

    private void HandleHealthRestored(float percent)
    {
        if (gameObject.activeSelf)
            _foregroundImage.fillAmount = percent;
    }

    /// <summary>
    /// Плавное процентное изменение здоровья
    /// </summary>
    /// <param name="percent"></param>
    /// <returns></returns>
    private IEnumerator ChangeToPercent(float percent)
    {
        float preChangePercent = _foregroundImage.fillAmount;
        float elapsed = 0f;

        while (elapsed < _updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            _foregroundImage.fillAmount = Mathf.Lerp(preChangePercent, percent, elapsed / _updateSpeedSeconds);
            yield return null;
        }

        _foregroundImage.fillAmount = percent;
    }
}
