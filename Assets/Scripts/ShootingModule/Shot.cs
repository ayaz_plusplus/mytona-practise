﻿using Assets.Scripts.UnitsModule;
using System.Collections;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [SerializeField]
    private LineRenderer _lineRenderer;

    [SerializeField]
    private float _fireDelay = 0.2f;

    private Unit _soldier;

    public Unit soldier { get => _soldier; set => _soldier = value; }

    public void Show(Vector3 from, Vector3 to)
    {
        soldier.StartCoroutine(FireTask(from, to));
    }

    private IEnumerator FireTask(Vector3 from, Vector3 to)
    {
        _lineRenderer.SetPositions(new Vector3[] { from, to });
        gameObject.SetActive(true);
        yield return new WaitForSeconds(_fireDelay);
        Destroy(gameObject);
    }
}
