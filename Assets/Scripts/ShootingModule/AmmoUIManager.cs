﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ShootingModule
{
    /// <summary>
    /// Управление визуальными компонентами боеприпасов
    /// </summary>
    public class AmmoUIManager : MonoBehaviour
    {
        [SerializeField]
        private AmmoHolder _ammoHolder;

        [SerializeField]
        private Text _ammoMainText;

        [SerializeField]
        private Button _ammoMainButton;

        [SerializeField]
        private Text _ammoSubText;

        [SerializeField]
        private Button _ammoSubButton;

        private Image _mainButtonImage;
        private Image _subButtonImage;

        private void Awake()
        {
            _ammoHolder.OnAmmoMainGunChanged += HandleAmmoChanged;
            _ammoHolder.OnGunChanged += HandleGunChanged;
            _ammoHolder.OnGunsInitializing += HandleInitialize;
            _ammoHolder.OnAmmoReload += HandleReload;
            _mainButtonImage = _ammoMainButton.GetComponent<Image>();
            _subButtonImage = _ammoSubButton.GetComponent<Image>();
        }

        private void OnDestroy()
        {
            _ammoHolder.OnAmmoMainGunChanged -= HandleAmmoChanged;
            _ammoHolder.OnGunChanged -= HandleGunChanged;
            _ammoHolder.OnGunsInitializing -= HandleInitialize;
            _ammoHolder.OnAmmoReload -= HandleReload;
        }

        private void HandleAmmoChanged(short amount)
        {
            _ammoMainText.text = amount.ToString();
        }

        private void HandleGunChanged(IWeapon weapon)
        {
            Sprite oldSprite = _mainButtonImage.sprite;
            string oldText = _ammoMainText.text;

            _mainButtonImage.sprite = weapon.gunData.icon;
            _ammoMainText.text = weapon.currentBulletAmount.ToString();

            _subButtonImage.sprite = oldSprite;
            _ammoSubText.text = oldText;
        }

        private void HandleInitialize(IWeapon mainWeapon, IWeapon subWeapon)
        {
            if (_ammoMainButton != null && _ammoSubButton != null)
            {
                _mainButtonImage.sprite = mainWeapon.gunData.icon;
                _ammoMainText.text = mainWeapon.currentBulletAmount.ToString();

                _subButtonImage.sprite = subWeapon.gunData.icon;
                _ammoSubText.text = subWeapon.currentBulletAmount.ToString();
            }           
        }

        private void HandleReload(IWeapon mainWeapon)
        {
            _ammoMainText.text = mainWeapon.currentBulletAmount.ToString();
        }
    }
}
