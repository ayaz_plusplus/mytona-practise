﻿using Assets.Scripts.MovementModule;
using Assets.Scripts.UnitsModule;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.ShootingModule
{
    /// <summary>
    /// Управление стрельбой ботов
    /// </summary>
    public class BotShooting : Shooting
    {
        [SerializeField]
        private float _fireDelay = 1f;

        [SerializeField]
        private BotMovement _botMovement;

        private bool _isBotFire;
        private Unit _unit;

        public override Vector3 targetPosition
        {
            get
            {
                return AdjustSight(UnitsHolder.GetPlayer().transform.position * Random.Range(1f, 1.3f));
            }
        }

        protected override bool IsFire
        {
            get
            {
                return _isBotFire;
            }
        }

        protected override void Start()
        {
            base.Start();
            _unit = GetComponent<Unit>();
            StartCoroutine(FireTask());
        }

        private IEnumerator FireTask()
        {
            for (; ; )
            {
                if (_unit.health != null && !_unit.health.isDeath && _botMovement.enemySightRange)
                {
                    _isBotFire = true;
                }
                yield return new WaitForSeconds(_fireDelay);
                _isBotFire = false;

            }
        }

        public void LookAt(Vector3 target)
        {
            Vector3 forward = target - transform.position;
            transform.rotation = Quaternion.LookRotation(new Vector3(forward.x, 0, forward.z));
        }
    }

}
