﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.ShootingModule
{
    /// <summary>
    /// Управление боеприсами выбранных оружий
    /// </summary>
    public class AmmoHolder : MonoBehaviour
    {
        private IWeapon _currentWeapon;
        private IWeapon _subWeapon;

        public IWeapon currentWeapon { get => _currentWeapon; set => _currentWeapon = value; }
        public IWeapon subWeapon { get => _subWeapon; set => _subWeapon = value; }

        public event Action<short> OnAmmoMainGunChanged = delegate { };
        public event Action<IWeapon> OnAmmoReload = delegate { };
        public event Action<IWeapon> OnGunChanged = delegate { };
        public event Action<IWeapon, IWeapon> OnGunsInitializing = delegate { };

        /// <summary>
        /// Изменить количество боеприпасов
        /// </summary>
        /// <param name="amount"></param>
        public void ChangeAmmoAmount(short amount)
        {
            currentWeapon.currentBulletAmount += amount;
            OnAmmoMainGunChanged(currentWeapon.currentBulletAmount);

            if (currentWeapon.currentBulletAmount == 0)
                Reload();
        }

        /// <summary>
        /// Сменить оружие
        /// </summary>
        public void ChangeWeapon()
        {
            IWeapon oldWeapon = currentWeapon;
            currentWeapon = subWeapon;
            subWeapon = oldWeapon;
            OnGunChanged(currentWeapon);
        }

        /// <summary>
        /// Инициализация оружий
        /// </summary>
        /// <param name="mainWeaponParam"></param>
        /// <param name="subWeaponParam"></param>
        public void GunsInitialize(IWeapon mainWeaponParam, IWeapon subWeaponParam)
        {
            currentWeapon = mainWeaponParam;
            subWeapon = subWeaponParam;

            currentWeapon.currentBulletAmount = currentWeapon.gunData.bulletAmount;
            subWeapon.currentBulletAmount = subWeaponParam.gunData.bulletAmount;
            OnGunsInitializing(mainWeaponParam, subWeaponParam);
        }

        public void Reload()
        {
            StartCoroutine(ReloadTask());
        }

        public IEnumerator ReloadTask()
        {
            yield return new WaitForSeconds(currentWeapon.gunData.reloadDelay);
            currentWeapon.currentBulletAmount = currentWeapon.gunData.bulletAmount;
            OnAmmoReload(currentWeapon);
        }
    }
}
