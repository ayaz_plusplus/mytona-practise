﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.ShootingModule.WeaponTypes
{
    public enum WeaponType
    {
        MachineGun = 1,
        ShotGun = 2,
        Rifle = 3,
        Pistol = 4,
        Grenade = 5
    }
}
