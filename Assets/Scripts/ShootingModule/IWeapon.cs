﻿using Assets.Scripts.UnitsModule;
using UnityEngine;

namespace Assets.Scripts.ShootingModule
{
    public interface IWeapon
    {
        GunData gunData { get; set; }

        short currentBulletAmount { get; set; }

        void Fire(Vector3 from, Vector3 to, Shot shot, Unit unit);
    }
}
