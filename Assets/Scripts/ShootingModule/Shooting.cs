﻿using Assets.Scripts.ShootingModule;
using Assets.Scripts.UnitsModule;
using UnityEngine;

public abstract class Shooting : MonoBehaviour
{
    [SerializeField]
    private float _aimRadiusCollisionDetected = 0.1f;

    [SerializeField]
    private LayerMask _groundLayer;

    [SerializeField]
    private Shot _shot;

    [SerializeField]
    private Transform _aim;

    [SerializeField]
    private Transform _target;

    [SerializeField]
    private ParticleSystem _muzzleFlash;

    public virtual Vector3 targetPosition { get => _target == null ? Vector3.zero : AdjustSight(_target.position); }

    [SerializeField]
    private AmmoHolder _ammoHolder;

    private Unit _currentUnit;
    private GunsContainer _gunsContrainer;
    private float _nextFire = 0f;

    private Vector3 _pointToFire;

    protected virtual bool IsFire
    {
        get => false;
    }
    public AmmoHolder ammoHolder { get => _ammoHolder; set => _ammoHolder = value; }
    public Transform aim { get => _aim; set => _aim = value; }
    public Vector3 pointFromFire { get => aim.position; }

    protected virtual void Start()
    {
        _currentUnit = GetComponent<Unit>();
        _gunsContrainer = GunsContainer.instance;
        _ammoHolder.GunsInitialize(_gunsContrainer.GetRandomGun(transform), _gunsContrainer.GetRandomGun(transform));
    }

    protected virtual void Update()
    {
        if (!_currentUnit.health.isDeath)
        {
            if (Time.time > _nextFire)
            {
                _nextFire = Time.time + 1f / ammoHolder.currentWeapon.gunData.fireRatePerSecond;
                Shoot();
            }

            if (Input.GetButtonDown("Fire2"))
                ammoHolder.ChangeWeapon();
        }
    }

    /// <summary>
    /// Стрельба
    /// </summary>
    protected virtual void Shoot()
    {
        if (IsFire && ammoHolder.currentWeapon != null)
        {
            if (ammoHolder.currentWeapon.gunData.bulletAmountOneShot > 1)
                MakeShot(true);
            else
                MakeShot(false);

        }
    }

    /// <summary>
    /// Вычислить точку мишени
    /// </summary>
    private bool CheckAimCollision()
    {
        bool aimInCollision = Physics.CheckSphere(pointFromFire, _aimRadiusCollisionDetected, _groundLayer);
        // проверка не находится ли наш прицел уткнутым в окружающую среду
        if (aimInCollision)
            _pointToFire = pointFromFire;
        else
        {
            var direction = (targetPosition - pointFromFire).normalized;
            _pointToFire = pointFromFire + direction * _ammoHolder.currentWeapon.gunData.distance;
        }

        return aimInCollision;
    }

    public Unit CheckCollision(Vector3 to, bool isSpread)
    {
        RaycastHit hit;
        Unit collision = null;

        if (_ammoHolder?.currentWeapon != null)
        {
            if (!CheckAimCollision())
            {
                Vector3 direction = to - pointFromFire;
                if (isSpread)
                    SpreadShotDirection(ref direction);

                if (Physics.Raycast(pointFromFire, direction, out hit, _ammoHolder.currentWeapon.gunData.distance))
                {
                    Debug.DrawLine(pointFromFire, hit.point, Color.green);
                    _pointToFire = AdjustSight(hit.point);

                    var unit = hit.transform.GetComponent<Unit>();
                    if (unit != null && unit.fractionData.id != _currentUnit.fractionData.id)
                        collision = unit;

                }
                else
                    Debug.DrawRay(pointFromFire, direction, Color.red, _ammoHolder.currentWeapon.gunData.distance);
            }
        }

        return collision;
    }

    /// <summary>
    /// Распределить выстрел дробью
    /// </summary>
    /// <param name="sourceDirection"></param>
    /// <returns></returns>
    private Vector3 SpreadShotDirection(ref Vector3 sourceDirection)
    {
        Vector3 spread = Vector3.zero;
        spread += transform.up * Random.Range(-1f, 1f);
        spread += transform.right * Random.Range(-1f, 1f);

        sourceDirection += spread.normalized * Random.Range(0f, 0.2f);
        return sourceDirection;
    }

    /// <summary>
    /// Логика одного выстрела
    /// </summary>
    /// <param name="hit"></param>
    private void MakeShot(bool isSpread)
    {
        if (ammoHolder.currentWeapon.currentBulletAmount > 0)
        {
            _currentUnit.soldierAnimator.ChangeAnimation(SoldierAnimator.SoldierAnimatorStates.Shoot);
            _muzzleFlash.Play();
            for (int i = 0; i < ammoHolder.currentWeapon.gunData.bulletAmountOneShot; i++)
            {

                var hitUnit = CheckCollision(targetPosition, isSpread);
                ammoHolder.currentWeapon.Fire(pointFromFire, _pointToFire, _shot, _currentUnit);

                if (hitUnit != null)
                    hitUnit.health.HitWeapon(_currentUnit, _ammoHolder.currentWeapon.gunData);
            }
            ammoHolder.ChangeAmmoAmount(-1);
        }
    }

    /// <summary>
    /// Скорректируем направление прицела по мушке
    /// </summary>
    /// <param name="sourceTarget"></param>
    /// <returns></returns>
    protected Vector3 AdjustSight(Vector3 sourceTarget)
    {
        sourceTarget.y = aim.position.y;
        return sourceTarget;
    }
}


