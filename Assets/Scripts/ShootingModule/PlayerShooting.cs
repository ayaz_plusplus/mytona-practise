﻿using UnityEngine;

namespace Assets.Scripts.ShootingModule
{
    /// <summary>
    /// Управление стрельбой игрока
    /// </summary>
    public class PlayerShooting : Shooting
    {
        /// <summary>
        /// Переопределение флага стрельбы - для игрока это нажатая кнопка стрельбы
        /// </summary>
        protected override bool IsFire
        {
            get
            {
                return Input.GetButton("Fire1");
            }
        }
    }
}
