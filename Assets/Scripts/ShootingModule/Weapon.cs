﻿using Assets.Scripts.ShootingModule.WeaponTypes;
using Assets.Scripts.UnitsModule;
using UnityEngine;

namespace Assets.Scripts.ShootingModule
{
    public abstract class Weapon : MonoBehaviour, IWeapon
    {
        private GunData _gunData;

        [SerializeField]
        private WeaponType _weaponType;

        private short _currentBulletAmount;

        public GunData gunData { get => _gunData; set => _gunData = value; }

        public short currentBulletAmount { get => _currentBulletAmount; set => _currentBulletAmount = value; }
        public WeaponType weaponType { get => _weaponType; set => _weaponType = value; }

        public virtual void Fire(Vector3 from, Vector3 to, Shot shot, Unit unit)
        {
            var newShot = Instantiate(shot);
            newShot.soldier = unit;
            newShot.Show(from, to);
        }
    }
}
