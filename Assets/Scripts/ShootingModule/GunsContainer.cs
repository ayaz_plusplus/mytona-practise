﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts.ShootingModule
{
    /// <summary>
    /// Источник данных вооружений
    /// </summary>
    public class GunsContainer : MonoBehaviour
    {
        public static GunsContainer instance;

        [SerializeField]
        private Weapon[] _weaponPrefabs;

        private GunData[] _guns;

        private const string gunsDataFolder = "GunData/";

        public GunData[] Guns { get => _guns; set => _guns = value; }

        private void Awake()
        {
            _guns = Resources.LoadAll<GunData>(gunsDataFolder);
            instance = this;
        }

        /// <summary>
        /// Получить префаб оружия соответствующий типу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Weapon GetGunById(int id, Transform parent)
        {
            var gunData = _guns.Where(gun => gun.id == id).FirstOrDefault();
            var weaponPrefab = _weaponPrefabs.Where(weapon => weapon.weaponType == gunData.weaponType).FirstOrDefault();
            weaponPrefab = Instantiate(weaponPrefab, parent);
            weaponPrefab.GetComponent<Weapon>().gunData = gunData;
            return weaponPrefab;
        }

        /// <summary>
        /// TODO Debug Метод для получения случайно оружия, в будущем id будет приниматься из другой сцены
        /// Ответственность за уникальность оружия будет брать "UI Picker" из этой же сцены выбора оружия игрока
        /// </summary>
        /// <returns></returns>
        public Weapon GetRandomGun(Transform parent)
        {
            return GetGunById(Random.Range(1, Guns.Length + 1), parent);
        }
    }
}
