﻿using System.Linq;
using UnityEngine;

/// <summary>
/// Класс содержащий все фракции
/// </summary>
public class FractionsContainer : MonoBehaviour
{
    private FractionData[] _fractions;

    public static FractionsContainer instance;
    private const string fractionsFolder = "FractionData/";

    public FractionData[] fractions { get => _fractions; set => _fractions = value; }

    private void Awake()
    {
        _fractions = Resources.LoadAll<FractionData>(fractionsFolder);
    }

    public FractionData GetFraction(int id)
    {
        return _fractions.Where(fraction => fraction.id == id).FirstOrDefault();
    }
}
