﻿using UnityEngine;

/// <summary>
/// Класс отвечающий за анимацию солдата
/// </summary>
public class SoldierAnimator : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void ChangeAnimation(SoldierAnimatorStates stateAnimation)
    {
        if (animator == null)
            animator = GetComponent<Animator>();

        animator.SetInteger("State", (int)stateAnimation);
    }

    public enum SoldierAnimatorStates
    {
        Idle = 0,
        Run = 1,
        Shoot = 2
    }
}
