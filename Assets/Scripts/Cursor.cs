﻿using UnityEngine;

/// <summary>
/// Курсор текущего игрока
/// </summary>
public class Cursor : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private LayerMask _groundLayer;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        float maxSearchDistance = 1000;

        if (!Physics.Raycast(ray, out hit, maxSearchDistance, _groundLayer))
            _spriteRenderer.enabled = false;
        else
        {
            transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            _spriteRenderer.enabled = true;
        }
    }
}
