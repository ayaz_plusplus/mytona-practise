﻿using Assets.Scripts.EventsModule;
using Assets.Scripts.Scriptable_Objects;
using Assets.Scripts.UnitsModule;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Управление лентой событий
/// </summary>
public class EventsPublisher : MonoBehaviour
{
    [SerializeField]
    private Notification _notificationPrefab;

    [SerializeField]
    private Notification _notificationMurderPrefab;

    [SerializeField]
    private int _maxNotificationCount = 3;

    public static EventsPublisher instance;

    private readonly Queue<Notification> notifications = new Queue<Notification>();

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Добавить информационное сообщение на ленту событий
    /// </summary>
    /// <param name="unit">Объект о котором сообщается событие</param>
    /// <param name="info">Текстовая информация</param>
    public void AddInfo(Unit unit, string info)
    {
        var notification = Instantiate(_notificationPrefab, transform);
        SetFractionText(notification.firstText, unit, info);
        AddNotification(notification);
    }

    /// <summary>
    /// Добавить информационное сообщение об убийстве другого игрока
    /// </summary>
    /// <param name="killer">Убийца</param>
    /// <param name="murdered">Убитый</param>
    /// <param name="weaponData">Информация об оружии</param>
    public void AddMurderInfo(Unit killer, Unit murdered, IWeaponData weaponData)
    {
        var notification = Instantiate(_notificationMurderPrefab, transform);

        SetFractionText(notification.firstText, killer, killer.nickname);
        SetFractionText(notification.secondText, murdered, murdered.nickname);

        notification.icon.sprite = weaponData.icon;
        AddNotification(notification);
    }

    /// <summary>
    /// Настроить стиль текста по фракции игрока
    /// </summary>
    /// <param name="text"></param>
    /// <param name="unit"></param>
    /// <param name="info"></param>
    private void SetFractionText(Text text, Unit unit, string info)
    {
        text.text = info;
        text.color = unit.fractionData.color;
    }

    /// <summary>
    /// Добавить новое уведомление в ленту событий
    /// </summary>
    /// <param name="notification"></param>
    private void AddNotification(Notification notification)
    {
        if (notifications.Count == _maxNotificationCount)
        {
            var notificationToDelete = notifications.Peek();
            notifications.Dequeue();
            if (notificationToDelete != null)
                Destroy(notificationToDelete.gameObject);
        }

        notifications.Enqueue(notification);
    }
}
