﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.EventsModule
{
    public class Notification : MonoBehaviour
    {
        [SerializeField]
        private float lifeTime = 3f;

        [SerializeField]
        private Text _firstText;

        [SerializeField]
        private Text _secondText;

        [SerializeField]
        private Image _icon;

        public Text firstText { get => _firstText; set => _firstText = value; }
        public Text secondText { get => _secondText; set => _secondText = value; }
        public Image icon { get => _icon; set => _icon = value; }

        private void Start()
        {
            Destroy(gameObject, lifeTime);
        }
    }
}
