﻿using UnityEngine;

namespace Assets.Scripts.Scriptable_Objects
{
    public interface IWeaponData
    {
        int id { get; set; }
        float damage { get; set; }
        float reloadDelay { get; set; }
        float distance { get; set; }
        Sprite icon { get; set; }
    }
}
