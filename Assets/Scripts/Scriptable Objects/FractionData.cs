﻿using UnityEngine;

/// <summary>
/// Источник данных фракций
/// </summary>
[CreateAssetMenu(fileName = "New Fraction", menuName = "Fraction Data", order = 51)]
public class FractionData : ScriptableObject
{
    [SerializeField]
    private int _id;

    [SerializeField]
    private string _fractionName;

    [SerializeField]
    private Color _color;

    public int id { get => _id; set => _id = value; }
    public string fractionName { get => _fractionName; set => _fractionName = value; }
    public Color color { get => _color; set => _color = value; }
}
