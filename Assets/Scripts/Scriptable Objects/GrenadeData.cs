﻿using Assets.Scripts.Scriptable_Objects;
using UnityEngine;

/// <summary>
/// Источник данных гранат
/// </summary>
[CreateAssetMenu(fileName = "New Grenade", menuName = "Grenade Data", order = 51)]
public class GrenadeData : ScriptableObject, IWeaponData
{
    [SerializeField]
    private int _id;

    [SerializeField]
    private float _damage;

    [SerializeField]
    private float _reloadDelay;

    [SerializeField]
    private float _triggerTime;

    [SerializeField]
    private float _distance;

    [SerializeField]
    private Sprite _icon;

    public int id { get => _id; set => _id = value; }
    public float damage { get => _damage; set => _damage = value; }
    public float reloadDelay { get => _reloadDelay; set => _reloadDelay = value; }
    public float triggerTime { get => _triggerTime; set => _triggerTime = value; }
    public float distance { get => _distance; set => _distance = value; }
    public Sprite icon { get => _icon; set => _icon = value; }
}
