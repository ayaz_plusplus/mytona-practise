﻿using Assets.Scripts.Scriptable_Objects;
using Assets.Scripts.ShootingModule.WeaponTypes;
using UnityEngine;

/// <summary>
/// Источник данных вооружений
/// </summary>
[CreateAssetMenu(fileName = "New Gun", menuName = "Gun Data", order = 51)]
public class GunData : ScriptableObject, IWeaponData
{
    [SerializeField]
    private int _id;

    [SerializeField, Range(0f, 100f)]
    private float _fireRatePerSecond;

    [SerializeField]
    private float _reloadDelay;

    [SerializeField]
    private float _damage;

    [SerializeField]
    private float _distance;

    [SerializeField]
    private short _bulletAmount;

    [SerializeField]
    private byte _bulletAmountOneShot;

    [SerializeField]
    private WeaponType _weaponType;

    [SerializeField]
    private Sprite _icon;

    public int id { get => _id; set => _id = value; }

    /// <summary>
    /// Скорострельность - количество выстрелов в секунду
    /// </summary>
    public float fireRatePerSecond { get => _fireRatePerSecond; set => _fireRatePerSecond = value; }

    public float reloadDelay { get => _reloadDelay; set => _reloadDelay = value; }

    public float damage { get => _damage; set => _damage = value; }

    public float distance { get => _distance; set => _distance = value; }

    public short bulletAmount { get => _bulletAmount; set => _bulletAmount = value; }

    public byte bulletAmountOneShot { get => _bulletAmountOneShot; set => _bulletAmountOneShot = value; }

    public Sprite icon { get => _icon; set => _icon = value; }
    public WeaponType weaponType { get => _weaponType; set => _weaponType = value; }
}
