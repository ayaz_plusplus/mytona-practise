﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GrenadeModule
{
    /// <summary>
    /// Класс отвечающий за откат гранаты
    /// </summary>
    public class GrenadeCountdownBar : MonoBehaviour
    {
        [SerializeField]
        private float _updateSpeedSeconds = 0.5f;

        [SerializeField]
        private Grenader _grenader;

        private Image _foregroundImage;

        private void Awake()
        {
            _grenader.OnCountdownChanged += HandleCountdownChanged;
            _foregroundImage = GetComponent<Image>();
        }

        private void OnDestroy()
        {
            _grenader.OnCountdownChanged -= HandleCountdownChanged;
        }

        private void HandleCountdownChanged(float percent)
        {
            if (gameObject.activeSelf)
                StartCoroutine(ChangeToPercent(percent));
        }

        /// <summary>
        /// Плавное изменение индикатора отката
        /// </summary>
        /// <param name="percent"></param>
        /// <returns></returns>
        private IEnumerator ChangeToPercent(float percent)
        {
            float preChangePercent = _foregroundImage.fillAmount;
            float elapsed = 0f;

            while (elapsed < _updateSpeedSeconds)
            {
                elapsed += Time.deltaTime;
                _foregroundImage.fillAmount = Mathf.Lerp(preChangePercent, percent, elapsed / _updateSpeedSeconds);
                yield return null;
            }

            _foregroundImage.fillAmount = percent;
        }
    }
}
