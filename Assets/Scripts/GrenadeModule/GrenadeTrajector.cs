﻿using UnityEngine;

/// <summary>
/// Траектория гранаты отрисовывается при подготовки броска
/// </summary>
public class GrenadeTrajector : MonoBehaviour
{

    [SerializeField]
    private int _maxSegmentCount = 300;

    private LineRenderer _lineRendererComponent;
    private Vector3[] _segments;
    private Vector3 _velocity;
    private Vector3 _gravity;
    private float _timeStep;
    private float _stepDrag;

    private void Start()
    {
        _lineRendererComponent = GetComponent<LineRenderer>();
        _segments = new Vector3[_maxSegmentCount];
    }

    /// <summary>
    /// Симуляция пути полета снаряда
    /// </summary>
    /// <param name="aimPosition"></param>
    /// <param name="forceDirection"></param>
    /// <param name="rigidbody"></param>
    public void SimulatePath(Vector3 aimPosition, Vector3 forceDirection, Rigidbody rigidbody)
    {
        _timeStep = Time.fixedDeltaTime;

        _stepDrag = 1 - rigidbody.drag * _timeStep;
        _velocity = forceDirection * _timeStep;
        _gravity = Physics.gravity * _timeStep * _timeStep;

        for (int i = 0; i < _maxSegmentCount; i++)
        {
            _velocity += _gravity;
            _velocity *= _stepDrag;

            aimPosition += _velocity;
            _segments[i] = aimPosition;
        }

        _lineRendererComponent.positionCount = _maxSegmentCount;
        for (int i = 0; i < _maxSegmentCount; i++)
        {
            _lineRendererComponent.SetPosition(i, _segments[i]);
        }
    }

    public void HideTrajectory()
    {
        _lineRendererComponent.positionCount = 0;
    }
}
