﻿using Assets.Scripts.UnitsModule;
using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Управление гранатой
/// </summary>
public class Grenader : MonoBehaviour
{
    [SerializeField]
    private GrenadeTrajector _grenadeTrajector;

    [SerializeField]
    private Grenade _grenadePrefab;

    [SerializeField]
    private Rigidbody _grenadeRigidBody;

    [SerializeField]
    private Transform _aim;

    [SerializeField]
    private Transform _target;

    [SerializeField]
    private Unit _unit;

    [SerializeField]
    private float _angleInDegrees = 45f;

    private Vector3 _direction;
    private float _angleInRadians;
    private float _xVelocity;
    private float _yVelocity;
    private float _velocityPow2;

    private readonly float g = Physics.gravity.y;
    private float _currentCountDownTime;

    public float currentCountDownTime
    {
        get => _currentCountDownTime;
        set
        {
            _currentCountDownTime = value;
            CountdownChange();
        }
    }

    public event Action<float> OnCountdownChanged = delegate { };

    private void Start()
    {
        _aim.localEulerAngles = new Vector3(-_angleInDegrees, 0f, 0f);
        _angleInRadians = _angleInDegrees * Mathf.PI / 180;
        currentCountDownTime = 0f;
    }

    private void Update()
    {
        if (Input.GetButton("Fire3") && currentCountDownTime <= 0)
            _grenadeTrajector.SimulatePath(_aim.position, _aim.forward * CalculateThrowVelocity(), _grenadeRigidBody);
        else
            _grenadeTrajector.HideTrajectory();

        if (Input.GetButtonUp("Fire3") && currentCountDownTime <= 0)
            StartCoroutine(ThrowGrenade());
    }


    /// <summary>
    /// Бросок гранаты
    /// </summary>
    /// <returns></returns>
    public IEnumerator ThrowGrenade()
    {
        var newGrenade = Instantiate(_grenadePrefab, _aim.position, _aim.rotation);
        newGrenade.unit = _unit;
        newGrenade.grenadeRigidbody.velocity = _aim.forward * CalculateThrowVelocity();

        currentCountDownTime = _grenadePrefab.grenadeData.reloadDelay;
        while (currentCountDownTime > 0)
        {
            currentCountDownTime -= Time.deltaTime;
            yield return null;
        }
    }

    /// <summary>
    /// Расчитать траекторию полета при помощи баллистической формулы
    /// </summary>
    /// <returns></returns>
    private float CalculateThrowVelocity()
    {
        _direction = CheckDistanceTarget(_target.position, _aim.position);
        _xVelocity = _direction.magnitude;
        _yVelocity = _direction.y;
        _velocityPow2 = (g * Mathf.Pow(_xVelocity, 2)) / (2 * (_yVelocity - Mathf.Tan(_angleInRadians) * _xVelocity) * Mathf.Pow(Mathf.Cos(_angleInRadians), 2));
        return Mathf.Sqrt(Mathf.Abs(_velocityPow2));
    }

    /// <summary>
    /// Ограничить дистанцию гранаты исходя из ее параметров
    /// </summary>
    /// <param name="sourceTarget"></param>
    /// <param name="aim"></param>
    /// <returns></returns>
    private Vector3 CheckDistanceTarget(Vector3 sourceTarget, Vector3 aim)
    {
        Vector3 direction = sourceTarget - aim;
        if (direction.magnitude > _grenadePrefab.grenadeData.distance)
        {
            direction = (sourceTarget - aim).normalized;
            sourceTarget = aim + direction * _grenadePrefab.grenadeData.distance;
            direction = sourceTarget - aim;
        }

        return direction;
    }

    /// <summary>
    /// Изменение в процентном соотношении отката
    /// </summary>
    private void CountdownChange()
    {
        float currentCountdownPercent = currentCountDownTime / _grenadePrefab.grenadeData.reloadDelay;
        OnCountdownChanged(currentCountdownPercent);
    }
}
