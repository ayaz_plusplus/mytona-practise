﻿using Assets.Scripts.UnitsModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Описание гранаты
/// </summary>
public class Grenade : MonoBehaviour
{
    [SerializeField]
    private GrenadeData _grenadeData;

    [SerializeField]
    private ParticleSystem _explotionEffect;

    [SerializeField]
    private Unit _unit;
    private Rigidbody _rigidbody;
    private List<Unit> explodedUnits = new List<Unit>();

    public Unit unit { get => _unit; set => _unit = value; }
    public Rigidbody grenadeRigidbody { get => _rigidbody; set => _rigidbody = value; }
    public GrenadeData grenadeData { get => _grenadeData; set => _grenadeData = value; }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        StartCoroutine(Explode());
    }

    /// <summary>
    /// Имитация взрыва
    /// </summary>
    /// <returns></returns>
    private IEnumerator Explode()
    {
        yield return new WaitForSeconds(grenadeData.triggerTime);
        _explotionEffect.Play();

        yield return new WaitForSeconds(_explotionEffect.main.duration);
        Destroy(gameObject);
    }

    private void HitGrenade(Health health)
    {
        health.HitWeapon(unit, grenadeData);
    }

    /// <summary>
    /// Обработка коллизий частиц после взрыва
    /// </summary>
    /// <param name="other"></param>
    private void OnParticleCollision(GameObject other)
    {
        var explosionUnit = other.GetComponent<Unit>();
        if (explosionUnit != null && !explodedUnits.Contains(explosionUnit))
        {
            HitGrenade(explosionUnit.health);
            explodedUnits.Add(explosionUnit);
        }       
    }
}
